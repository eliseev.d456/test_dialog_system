FROM python:3.9
ENV POETRY_VERSION=1.1.11
ENV ENVIRONMENT=DEV
# RUN DEBIAN_FRONTEND=noninteractive apt-get update -y && apt-get install -y ffmpeg

RUN pip install "poetry==$POETRY_VERSION"
WORKDIR /source
COPY  ./source/poetry/poetry.lock ./source/poetry/pyproject.toml  /source/

RUN poetry config virtualenvs.create false
RUN poetry install -vvv

COPY ./source/ /source
COPY ./.env /source

CMD ["poetry", "run", "uvicorn", "app:app", "--host=0.0.0.0", "--port=9998", "--reload"]