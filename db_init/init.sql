CREATE TABLE users (
    id INT PRIMARY KEY NOT NULL,
    time_first_message TIME NOT NULL
);

CREATE TABLE messages (
    id serial PRIMARY KEY NOT NULL,
	user_id INT REFERENCES users (id),
	message VARCHAR ( 200 ) NOT NULL,
	time_send TIME NOT NULL,
	correct BOOLEAN NOT NULL
);
