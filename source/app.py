from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from backend.router import router as api_router

app = FastAPI(title="Test dialogue systems")

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["POST"],
    allow_headers=["Content-Type", "Accept", "Origin"],
)

app.include_router(api_router)
