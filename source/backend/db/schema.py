import sqlalchemy as sa
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql import func

Base = declarative_base()


class User(Base):
    __tablename__ = "users"
    id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    time_first_message = sa.Column(sa.DateTime, default=func.now(), nullable=False)


class Message(Base):
    __tablename__ = "messages"
    id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    message = sa.Column(sa.String(200), nullable=False)
    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), nullable=False)
    time_send = sa.Column(sa.DateTime, default=func.now(), nullable=False)
    correct = sa.Column(sa.Boolean, nullable=False)

