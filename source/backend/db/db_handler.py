from contextlib import contextmanager

import sqlalchemy as sa
from sqlalchemy.orm import sessionmaker

from backend.db.schema import Base
from backend.db.schema import Message
from backend.db.schema import User


class DBHandler:

    def __init__(self, user_name, user_password, db_url, db_port, db_name):
        self._main_engine = sa.create_engine(
            f"postgresql+psycopg2://{user_name}:{user_password}@{db_url}:{db_port}/{db_name}",
            echo=True,
        )

        self._DBSession = sessionmaker(
            binds={
                Base: self._main_engine,
            },
            expire_on_commit=False,
        )

    @contextmanager
    def session_scope(self):
        session = self._DBSession()
        try:
            yield session
            session.commit()
        except Exception as e:
            session.rollback()
            raise e
        finally:
            session.close()

    def add_message(self, message: str, user_id: int, correct: bool):
        with self.session_scope() as sess:
            new_message = Message(message=message, user_id=user_id, correct=correct)
            sess.add(new_message)

    def add_user(self, user_id):
        with self.session_scope() as sess:
            new_user = User(id=user_id)
            sess.add(new_user)


class DBError(Exception):
    pass
