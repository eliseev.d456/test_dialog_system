from pathlib import Path

import environs

PROJECT_DIR = Path(__file__).parent.parent.resolve()

env = environs.Env()
env.read_env(PROJECT_DIR / ".env", recurse=False)

DB_USER_NAME = env.str("DB_USER_NAME")
DB_USER_PASSWORD = env.str("DB_USER_PASSWORD")
DB_URL = env.str("DB_URL")
DB_PORT = env.str("DB_PORT")
DB_NAME = env.str("DB_NAME")
