from fastapi import APIRouter
from fastapi import HTTPException
from pydantic import BaseModel

from .controller import Controller
from .db.db_handler import DBError
from .db.db_handler import DBHandler
from .message_handler import MessageError
from .settings import DB_NAME
from .settings import DB_PORT
from .settings import DB_URL
from .settings import DB_USER_NAME
from .settings import DB_USER_PASSWORD

db_handler = DBHandler(user_name=DB_USER_NAME, user_password=DB_USER_PASSWORD, db_url=DB_URL, db_port=DB_PORT,
                       db_name=DB_NAME)
controller = Controller(db_handler)
router = APIRouter(prefix="/api")


class MessagePayload(BaseModel):
    message: str
    user_id: str


@router.post("/send_message")
async def send_message(message_payload: MessagePayload) -> str:
    try:
        user_id = int(message_payload.user_id)
    except ValueError:
        raise HTTPException(
            status_code=500,
            detail=f"User id must be integer. Got {message_payload.user_id}"
        )
    message = message_payload.message
    try:
        controller.message_processing(user_id, message)
        message_to_user = controller.get_message_to_user(user_id)
    except (MessageError, DBError) as ex:
        raise HTTPException(
            status_code=500,
            detail=str(ex)
        )
    return message_to_user
