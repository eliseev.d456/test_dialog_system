from typing import Dict
from typing import Optional

import sqlalchemy as sa

from .db.db_handler import DBError
from .db.db_handler import DBHandler
from .message_handler import MessageError
from .message_handler import convert_message
from .message_handler import converter_message
from .response_tree import ResponseTree
from .response_tree import State


class Controller:
    def __init__(self, db_handler: DBHandler):
        self._id2state: Dict[int, State] = dict()
        self._response_tree: ResponseTree = ResponseTree()
        self._db_handler = db_handler

    @staticmethod
    def _prepare_message(message: str) -> Optional[str]:
        return convert_message(message)

    def _go_to_start(self, user_id: int):
        self._go_to_state(user_id, State.START)

    def _go_to_state(self, user_id: int, state: State):
        self._id2state[user_id] = state
        self._response_tree.go_to_state(state)

    def message_processing(self, user_id: int, message: str) -> None:
        is_correct_message = True
        if self._is_first_message(user_id):
            try:
                self._go_to_start(user_id)
                self._db_handler.add_user(user_id)
                if message != "/start":
                    is_correct_message = False
                    self._db_handler.add_message(message, user_id, is_correct_message)
                    raise MessageError("The first user message should be '/start'")
                self._db_handler.add_message(message, user_id, is_correct_message)
            except sa.exc.SQLAlchemyError as ex:
                self._remove_user(user_id)
                raise DBError(str(ex))

        elif message == "/start":
            self._db_handler.add_message(message, user_id, is_correct_message)
            self._go_to_start(user_id)
        else:
            self._go_to_state(user_id, self._id2state[user_id])
            preproc_message = self._prepare_message(message)
            if preproc_message is None:
                is_correct_message = False
                self._db_handler.add_message(message, user_id, is_correct_message)
                raise MessageError(
                    f"Message in the wrong format. Possible formats {converter_message.keys()}. Got {message}"
                )
            self._db_handler.add_message(preproc_message, user_id, is_correct_message)
            self._next_step(user_id, preproc_message)

    def get_message_to_user(self, user_id) -> str:
        state = self._id2state[user_id]
        self._go_to_state(user_id, state)
        msg = self._response_tree.get_message()
        self._db_handler.add_message(msg, user_id, True)
        return msg

    def _remove_user(self, user_id):
        if user_id in self._id2state:
            del self._id2state[user_id]

    def _is_first_message(self, user_id):
        return user_id not in self._id2state

    def _next_step(self, user_id, message) -> None:
        self._response_tree.next_step(message)
        state = self._response_tree.get_state()
        self._id2state[user_id] = state
