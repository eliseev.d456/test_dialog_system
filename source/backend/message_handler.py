from typing import Optional

converter_message = {
    "да": "да",
    "ага": "да",
    "конечно": "да",
    "пожалуй": "да",
    "нет": "нет",
    "нет, конечно": "нет",
    "ноуп": "нет",
    "найн": "нет",
}


class MessageError(Exception):
    pass


def convert_message(message: str) -> Optional[str]:
    try:
        return converter_message[message.lower()]
    except KeyError:
        return None
