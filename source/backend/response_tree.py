from enum import Enum
from typing import Literal
from typing import Optional

from backend.message_handler import MessageError

START_MESSAGE = "Привет! Я помогу отличить кота от хлеба! Объект перед тобой квадратный?"
MESSAGE_ABOUT_EARS = "У него есть уши?"
MESSAGE_IT_IS_BREAD = "Это хлеб, а не кот! Ешь его!"
MESSAGE_IT_IS_CAT = "Это кот, а не хлеб! Не ешь его!"


class State(Enum):
    START = 0
    ABOUT_EARS = 1
    IT_IS_BREAD = 2
    IT_IS_CAT = 3


class Node:
    def __init__(self, message: str, state: State, left_child: Optional["Node"] = None,
                 right_child: Optional["Node"] = None):
        self._message = message
        self._state = state
        self._left_child = left_child
        self._right_child = right_child

    @property
    def message(self) -> str:
        return self._message

    @property
    def left_child(self) -> Optional["Node"]:
        return self._left_child

    @property
    def right_child(self) -> Optional["Node"]:
        return self._right_child

    @property
    def state(self) -> State:
        return self._state


class ResponseTree:
    def __init__(self, state: Optional[State] = None):

        self._it_is_bread_node: Node = Node(message=MESSAGE_IT_IS_BREAD, state=State.IT_IS_BREAD)
        self._it_is_cat_node: Node = Node(message=MESSAGE_IT_IS_CAT, state=State.IT_IS_CAT)
        self._about_ears_node: Node = Node(message=MESSAGE_ABOUT_EARS, left_child=self._it_is_cat_node,
                                           right_child=self._it_is_bread_node, state=State.ABOUT_EARS)
        self._start_node: Node = Node(message=START_MESSAGE, left_child=self._about_ears_node,
                                      right_child=self._it_is_cat_node, state=State.START)
        self._head = None
        if state:
            self.go_to_state(state)
        else:
            self._head = self._start_node

    def get_message(self) -> str:
        return self._head.message

    def get_state(self) -> State:
        return self._head.state

    def next_step(self, message: Literal["да", "нет"]) -> None:
        if message == "да":
            if self._head.left_child is None:
                raise MessageError("Can't give an answer")
            self._head = self._head.left_child
        else:
            if self._head.right_child is None:
                raise MessageError("Can't give an answer")
            self._head = self._head.right_child

    def go_to_state(self, state: State) -> None:
        if state == State.START:
            self._head = self._start_node
        elif state == State.ABOUT_EARS:
            self._head = self._about_ears_node
        elif state == State.IT_IS_CAT:
            self._head = self._it_is_cat_node
        elif state == State.IT_IS_BREAD:
            self._head = self._it_is_bread_node
