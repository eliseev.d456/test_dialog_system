from backend.message_handler import MessageError
from backend.response_tree import MESSAGE_ABOUT_EARS
from backend.response_tree import MESSAGE_IT_IS_BREAD
from backend.response_tree import MESSAGE_IT_IS_CAT
from backend.response_tree import ResponseTree
from backend.response_tree import START_MESSAGE
from backend.response_tree import State


def test_response_tree_init():
    tree = ResponseTree()
    if tree.get_message() != START_MESSAGE:
        raise ValueError('Wrong message')
    tree = ResponseTree()
    if tree.get_message() != START_MESSAGE:
        raise ValueError('Wrong message')
    tree = ResponseTree(State.ABOUT_EARS)
    if tree.get_message() != MESSAGE_ABOUT_EARS:
        raise ValueError('Wrong message')
    tree = ResponseTree(State.IT_IS_BREAD)
    if tree.get_message() != MESSAGE_IT_IS_BREAD:
        raise ValueError('Wrong message')
    tree = ResponseTree(State.IT_IS_CAT)
    if tree.get_message() != MESSAGE_IT_IS_CAT:
        raise ValueError('Wrong message')


def test_logic_of_response_tree():
    tree = ResponseTree()
    yes_yes(tree)

    tree = ResponseTree()
    yes_no(tree)

    tree = ResponseTree()
    no(tree)

    tree.go_to_state(State.START)
    yes_no(tree)

    tree.go_to_state(State.ABOUT_EARS)
    tree.next_step("да")
    if tree.get_message() != MESSAGE_IT_IS_CAT and tree.get_state() != State.IT_IS_CAT:
        raise ValueError('Wrong message')

    tree.go_to_state(State.ABOUT_EARS)
    tree.next_step("нет")
    if tree.get_message() != MESSAGE_IT_IS_BREAD and tree.get_state() != State.IT_IS_BREAD:
        raise ValueError('Wrong message')

    tree.go_to_state(State.IT_IS_CAT)
    ok = False
    try:
        tree.next_step("да")
    except MessageError:
        ok = True
    if not ok:
        raise ValueError("expected error")

    try:
        tree.next_step("нет")
    except MessageError:
        ok = True
    if not ok:
        raise ValueError("expected error")


def yes_yes(tree):
    tree.next_step("да")
    if tree.get_message() != MESSAGE_ABOUT_EARS and tree.get_state() != State.ABOUT_EARS:
        raise ValueError('Wrong message')
    tree.next_step("да")
    if tree.get_message() != MESSAGE_IT_IS_CAT and tree.get_state() != State.IT_IS_CAT:
        raise ValueError('Wrong message')


def yes_no(tree):
    tree.next_step("да")
    if tree.get_message() != MESSAGE_ABOUT_EARS and tree.get_state() != State.ABOUT_EARS:
        raise ValueError('Wrong message')
    tree.next_step("нет")
    if tree.get_message() != MESSAGE_IT_IS_BREAD and tree.get_state() != State.IT_IS_BREAD:
        raise ValueError('Wrong message')


def no(tree):
    tree.next_step("нет")
    if tree.get_message() != MESSAGE_IT_IS_CAT:
        raise ValueError('Wrong message')
