# test_dialog_system

Bot that distinguishes a cat from bread with the following logic

![alt text](./docs/images/response_tree.png)

## Getting started

To install the service, you need to install docker. After installing docker, you need to clone this repository

```
git clone git@gitlab.com:eliseev.d456/test_dialog_system.git
```

You also need to create a file .env and put it in a folder
./test_dialog_system
Example .env file

```commandline

DB_USER_NAME=postgres
DB_USER_PASSWORD=password4dialogsystem
DB_URL=postgres_container
DB_PORT=5432
DB_NAME=dialog_system_db

```

To deploy this service, you need to register commands

```commandline
docker compose build backend_service
docker compose up -d
```

## API

This application is implemented on fastapi and the swager of the working application is available at /docs

```
post /api/send
input 
{
  "message": "string",
  "user_id": "string"
}
output
"string"
```

The beginning of the dialogue begins with the message /start. After that, "да" or "нет" messages are expected. If the message "
/start" is entered, then the dialogue is thrown to the beginning.
The case of messages is not important.

Message equivalents "да": "конечно", "ага", "пожалуй"

Message equivalents "нет": "нет, конечно", "ноуп", "найн"

## Database
For the convenience of working with the database, pgAdmin 4 is deployed on port 5050

### Schema

![alt text](./docs/images/QuickDBD-Free Diagram.png)
